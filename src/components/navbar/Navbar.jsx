import { AppBar, Toolbar, IconButton, Badge, MenuItem, Menu, Typography } from "@material-ui/core"
import { Link } from 'react-router-dom'
import { Grid } from "@material-ui/core"

import logo from '../../assets/logo.jpeg'
import paw from '../../assets/paw.png'

import useStyles from './NavbarStyles'

const Navbar = ({ totalItems }) => {
    const classes = useStyles()
    return (
        <div>
            <AppBar position="fixed" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" className={classes.title} color="inherit">
                        <img src={logo} alt="2 Кота" height="48px" className={classes.image} />
                        2 Кота
                    </Typography>
                    <div className={classes.grow} />
                    <div>
                        <IconButton component={Link} to="/" color="inherit" className={classes.btn}>
                            Главная
                        </IconButton>

                        <IconButton component={Link} to="/shop" color="inherit" className={classes.btn}>
                            Товары
                        </IconButton>

                        <IconButton component={Link} to="/cart" color="inherit" className={classes.btn}>
                            Корзина
                            <Badge badgeContent={totalItems} color="secondary" className={classes.badge}>
                                <img src={paw} alt="" width="24px" />
                            </Badge>
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>

        </div>
    )
}

export default Navbar