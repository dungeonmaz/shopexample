import { InputLabel, Select, MenuItem, Button, Grid, Typography } from "@material-ui/core"
import { Link } from "react-router-dom"
import { useForm, FormProvider } from "react-hook-form"
import CustomTextField from "./CustomTextField"

const NameForm = ({next}) => {
  const methods = useForm()
  return (
    <div>
      <Typography variant="h6" gutterBottom> Данные </Typography>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit((data)=> next({ ...data}))}>
          <Grid container spacing={3}>
            <CustomTextField required name='firstName' label='Имя'/>
            <CustomTextField required name='secondName' label='Фамилия'/>
            <CustomTextField required name='phoneNumber' label='Телефон'/>
            <CustomTextField required name='email' label='Электронная почта'/>
            <Grid item xs={12} sm={6}>
              <InputLabel></InputLabel>
            </Grid>
          </Grid>

          <div style ={{display:'flex', justifyContent:'space-between'}}>
            <Button variant="outlined" component={Link} to='/cart'>К корзине</Button>
            <Button type='submit' variant="contained" color='primary'>Дальше</Button>
          </div>

        </form>
      </FormProvider>
    </div>
  )
}

export default NameForm