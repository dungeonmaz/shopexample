import { Typography, Button, Divider } from "@material-ui/core"
import { Elements, CardElement, ElementsConsumer } from "@stripe/react-stripe-js"
import { loadStripe } from "@stripe/stripe-js"

import Review from './Review'

const stripePromise = loadStripe('...')

const PaymentForm = ({ checkoutToken, prevStep }) => {
  return (
    <div>
      <Review checkoutToken={checkoutToken} />
      <Divider />
      <Typography variant='h6' gutterBottom style={{ margin: '20px 0' }}>Способ оплаты</Typography>
      <Elements stripe={stripePromise}>
        <ElementsConsumer>
          {({ elements, stripe }) => (
            <form>
              <CardElement />
              <br></br>
              <br></br>
              <div style={{display:'flex', justifyContent:'space-between'}}>
                  <Button variant="outlined" onClick={prevStep}>Назад</Button>
                  <Button type="submit" variant="contained" color='primary' disabled={!stripe}>
                    Оплатить {checkoutToken.live.subtotal.formatted_with_symbol}
                  </Button>
              </div>
            </form>
          )}
        </ElementsConsumer>
      </Elements>
    </div>
  )
}

export default PaymentForm