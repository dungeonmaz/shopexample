import { commerce } from "../../lib/commerce"
import { Paper, Stepper, Step, StepLabel, Typography, CircularProgress, Divider, Button } from "@material-ui/core"
import { useEffect, useState } from "react"
import useStyles from './CheckoutStyles'
import NameForm from "./NameForm"
import PaymentForm from "./PaymentForm"


const steps = ['Данные', 'Детали оплаты']

const Checkout = ({ cart }) => {
    const [activeStep, setActiveStep] = useState(0)
    const [checkoutToken, setCheckoutToken] = useState(null)
    const [shippingData, setShippingData] = useState({})
    const classes = useStyles()

    useEffect(() => {
        const generateToken = async () => {
            try {
                const token = await commerce.checkout.generateToken(cart.id, { type: 'cart' })
                console.log(token)
                setCheckoutToken(token)
            } catch (error) {

            }

        }

        generateToken()
    }, [cart])

    const nextStep = () => setActiveStep((prevStep) => prevStep + 1)
    const prevStep = () => setActiveStep((prevStep) => prevStep - 1)

    const next = (data) => {
        setShippingData(data)
        nextStep()
    }

    const ConfirmationForm = () => {
        return (
            <div>
                Confirmation
            </div>
        )
    }

    const Form = () => {
        return (
            activeStep === 0
                ? <NameForm next = {next}/>
                : <PaymentForm checkoutToken={checkoutToken} prevStep = {prevStep} />
        )
    }

    return (
        <div>
            <div className={classes.toolbar} />
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography variant="h4" align="center">Оплата</Typography>
                    <Stepper activeStep={activeStep} className={classes.stepper}>
                        {steps.map((step) => (
                            <Step key={step}>
                                <StepLabel>{step}</StepLabel>
                            </Step>
                        ))}
                    </Stepper>
                    {activeStep === steps.length ? <ConfirmationForm /> : checkoutToken && <Form />}
                </Paper>
            </main>
        </div>
    )
}

export default Checkout