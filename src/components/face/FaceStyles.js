import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme)=>({
    toolbar: theme.mixins.toolbar,
    root:{
        margin:"4vw",
        display:'flex',
        flexDirection:'column',
    },
    name:{
        textAlign:'center'
    },
    description:{
        color:'gray'
    },
    info:{
        display:'flex',
        flexDirection:'column',
        width:'240px'
    },
    adress: {
        color: 'black',
        textDecoration:'none',
        "&:visited":{
            color:'chocolate' 
        }
    },
    footer:{
        background:'lightgray',
        display:'flex',
        justifyContent:'space-around',
        position:'fixed',
        width:'100%',
        bottom:0,
        left:0,
        borderTop:'2px solid rgba(0, 0, 0, 0.22)',
    },
}));