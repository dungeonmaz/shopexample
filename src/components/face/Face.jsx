import useStyles from './FaceStyles'

const Face = () => {
    const classes = useStyles()
    return (
        <div>
            <div className={classes.root}>
                <div className={classes.toolbar} />

                <div className={classes.info}>
                    <h2 className={classes.name}>2Кота</h2>
                    <p className={classes.description}>Авторские подарки,<br></br> настольные игры и манга</p>
                </div>
            </div>
            <div className={classes.footer}>
                <p><a href="https://go.2gis.com/rz52r8" className={classes.adress}>Проспект Ленина, 85А, Томск</a></p>
                <p>Телефон +7 </p>
            </div>
        </div>

    )
}

export default Face