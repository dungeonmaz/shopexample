import { Card, CardMedia, CardContent, CardActions, Typography, IconButton } from '@material-ui/core'

import useStyles from './ProductStyles'

import paw from '../../../assets/paw.png'

const Product = ({ product, onAddToCart }) => {
    const classes = useStyles()

    return (
        <Card className={classes.root}>
            <div>
                <CardMedia className={classes.media} image={product.image.url} title={product.name} />
                <CardContent>
                    <div className={classes.CardContent}>
                        <Typography variant='h5' gutterBottom>
                            {product.name}
                        </Typography>
                    </div>
                    <Typography dangerouslySetInnerHTML={{ __html: product.description }} variant='body2' color='textSecondary' />
                </CardContent>
            </div>
            <CardActions disableSpacing className={classes.cardActions}>
                <IconButton className={classes.btn} onClick={() => onAddToCart(product.id, 1)}>
                    {product.price.formatted_with_symbol}
                    <img src={paw} width='24px' alt='' className={classes.btnImage} />
                </IconButton>
            </CardActions>
        </Card>
    )
}

export default Product