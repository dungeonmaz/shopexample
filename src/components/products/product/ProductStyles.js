import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
    root: {
        borderRadius: '16px',
        height: '600px',
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-between'
    },
    media: {
        height:'200px',
        paddingTop: '56.25%', // 16:9
    },
    cardActions: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    cardContent: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    btn: {
        background: 'chocolate',
        borderRadius: '8px',
        transition:'0.5s',
        fontSize: '16px',
        padding:'8px',
        "&:hover":{
            background:'pink',
            transform:'scale(1.05)'
        }
    },

    btnImage:{
        marginLeft:'8px'
    }
}));