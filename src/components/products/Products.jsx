import { Grid } from '@material-ui/core'
import Product from './product/Product'

import useStyles from './ProductsStyles'

const Products = ({products, onAddToCart}) => {
    const classes = useStyles()
    return (
        <main className={classes.content}>
            <div className={classes.toolbar} />
            <Grid container justify='center' spacing={2}>
                {products.map(product => (
                    <Grid item key={product.id} xs={6} sm={4} md={3} lg={2}>
                        <Product product={product} onAddToCart={onAddToCart}/>
                    </Grid>
                ))}
            </Grid>
        </main>
    )
}

export default Products