import { Container, Typography, Button, Grid } from "@material-ui/core"
import CartItem from "./cartItem/CartItem"
import useStyles from "./CartStyles"
import { Link } from "react-router-dom"

const Cart = ({ cart, onUpdateCartQuantity, onRemoveFromCart, onEmptyCart }) => {
    const classes = useStyles()

    if (!cart.line_items) {
        return '...loading'
    }

    const EmptyCart = () => {
        return (
            <Typography variant="subtitle1">Пустая корзина</Typography>
        )
    }

    const FilledCart = () => {
        return (
            <div>
                <Grid container spacing={2}>
                    {cart.line_items.map((product) => (
                        <Grid item xs={12} sm={6} md={4} lg={3} key={product.id}>
                            {console.log(product.name)}
                            <CartItem product={product} onUpdateCartQuantity={onUpdateCartQuantity} onRemoveFromCart={onRemoveFromCart}/>
                        </Grid>
                    ))}
                </Grid>
                <div className={classes.cartDetails}>
                    <Typography variant="h4">
                        Общая цена: {cart.subtotal.formatted_with_symbol}
                    </Typography>
                    <div>
                        <Button className={classes.emptyButton} size="large" type="button" variant="contained" color="secondary" onClick={onEmptyCart}>Очистить</Button>
                        <Button component={Link} to ='/checkout' className={classes.checkoutButton} size="large" type="button" variant="contained" color="primary">Оплата</Button>
                    </div>
                </div>
            </div>)
    }

    return (
        <Container>
            <div className={classes.toolbar} />
            <Typography className={classes.title} variant='h3' gutterBottom>Ваши покупки</Typography>
            {!cart.line_items.length ? <EmptyCart /> : <FilledCart />}
        </Container>

    )
}

export default Cart