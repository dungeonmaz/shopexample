import useStyles from './CartItemStyles'
import { Button, Typography, Card, CardActions, CardContent, CardMedia } from '@material-ui/core'

const CartItem = ({product, onUpdateCartQuantity, onRemoveFromCart}) => {
    const classes = useStyles()
    return (
        <Card className={classes.root}>
            <div>
            <CardMedia image={product.image.url} alt={product.name} className={classes.media}/>
            <CardContent className={classes.cardContent}>
                <Typography variant='h5' gutterBottom>{product.name}</Typography>
                <Typography variant='body1' color='textSecondary'>{product.line_total.formatted_with_symbol}</Typography>
            </CardContent>
            </div>
            <CardActions className={classes.cardActions}>
                <div className={classes.buttons}>
                    <Button type="button" size="small" onClick={() => onUpdateCartQuantity(product.id, product.quantity - 1)}>-</Button>
                    <Typography>{product.quantity}</Typography>
                    <Button type="button" size="small" onClick={() => onUpdateCartQuantity(product.id, product.quantity + 1)}>+</Button>
                </div>
                <Button className={classes.btn} onClick={() => onRemoveFromCart(product.id)}>Удалить</Button>
            </CardActions>
        </Card>
    )
}

export default CartItem