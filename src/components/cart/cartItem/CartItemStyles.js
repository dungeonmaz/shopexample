import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    borderRadius: '16px',
    height: '600px',
    display:'flex',
    flexDirection:'column',
    justifyContent:'space-between'
  },
  media: {
    height: '200px',
    paddingTop: '60%',
  },
  cardActions: {
    display: 'flex',
    justifyContent:'space-between',
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'space-between',
    flexDirection:"column",
  },
  cartActions: {
    justifyContent: 'space-between',
  },
  buttons: {
    display: 'flex',
    alignItems: 'center',
    margin:"4px",
  },

  btn: {
    background: 'chocolate',
    borderRadius: '8px',
    transition:'0.5s',
    fontSize: '16px',
    padding:'8px',
    "&:hover":{
        background:'pink',
        transform:'scale(1.05)'
    }
},
}));