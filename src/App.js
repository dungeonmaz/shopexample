import { Products, Navbar, Cart, Face, Checkout } from './components'
import { commerce } from './lib/commerce'
import { useState, useEffect } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { CommentTwoTone } from '@material-ui/icons';


const App = () => {
    const [products, setProducts] = useState([])
    const [cart, setCart] = useState({})


    const fetchProducts = async () => {
        const { data } = await commerce.products.list();

        setProducts(data)
    }

    const fetchCart = async () => {
        setCart(await commerce.cart.retrieve())
    }

    const handleAddToCart = async (productId, quantity) => {
        const { cart } = await commerce.cart.add(productId, quantity)

        setCart(cart)
    }

    const handleUpdateCartQuantity = async (productId, quantity) => {
        const { cart } = await commerce.cart.update(productId, { quantity })

        setCart(cart)
    }

    const handleRemoveFromCart = async (productId) => {
        const { cart } = await commerce.cart.remove(productId)

        setCart(cart)
    }

    const handleEmptyCart = async () => {
        const { cart } = await commerce.cart.empty()

        setCart(cart)
    }

    useEffect(() => {
        fetchProducts()
        fetchCart()
    }, [])

    return (
        <BrowserRouter>
            <div>
                <Navbar totalItems={cart.total_items} />
                <Routes>
                    <Route path='/shop' element={<Products products={products} onAddToCart={handleAddToCart} />} />
                    <Route path='/cart' element={<Cart
                        cart={cart}
                        onUpdateCartQuantity={handleUpdateCartQuantity}
                        onRemoveFromCart={handleRemoveFromCart}
                        onEmptyCart={handleEmptyCart}
                    />} />
                    <Route path='/' element={<Face />} />
                    <Route path='/checkout' element={<Checkout cart={cart} />} />
                </Routes>
            </div>
        </BrowserRouter>
    )
}

export default App